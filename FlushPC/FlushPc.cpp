﻿// FlushPc emulator using a CPU 6502
// 

#include "FlushPc.h"


int main()
{
	Mem mem;
	CPU_6502 cpu;

	cpu.Reset( mem );
	//start
	mem[0xFFFC] = CPU_6502::INS_LDA_ZP;
	mem[0xFFFC] = 0x69;
	mem[0x0069] = 0x84;
	// end
	cpu.Execute( 3, mem );

	return 0;
}
