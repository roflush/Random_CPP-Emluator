#pragma once
#include <stdio.h>

using Byte = unsigned char;
using Word = unsigned short;
using u32  = unsigned int;

struct Mem {
	static constexpr u32 MAX_MEM = 1024 * 64;
	Byte Data[MAX_MEM];

	void Initialise()
	{
		for (u32 i = 0; i < MAX_MEM; i++)
		{
			Data[i] = 0;
		}
	}

	Byte operator[]( u32 Address ) const
	{
		return Data[Address];
	}

	// Write 1 byte
	Byte& operator[]( u32 Address )
	{
		return Data[Address];
	}
};


struct CPU_6502 {

	Word PC;
	Word SP;

	Byte A, 
		 X,
		 Y;

	Byte C		: 1;
	Byte Z		: 1;
	Byte I		: 1;
	Byte D		: 1;
	Byte B		: 1;
	Byte Unused : 1;
	Byte V		: 1; 
	Byte N		: 1;

	void Reset( Mem& memory ) 
	{
		PC = 0xFFFC;
		SP = 0x00FF;
		D  = 0;
		C  = Z = I = D = B = V = N = 0;
		A  = X = Y = 0;
		memory.Initialise();
	}

	Byte FetchByte(u32 Cycles, Mem& memory)
	{
		Byte Data = memory[PC];

		// Cycle execution
		PC++;
		Cycles--;
		
		return Data;
	}

	Byte ReadByte(u32& Cycles, Byte Address, Mem& memory)
	{
		Byte Data = memory[Address];
		Cycles--;
		return Data;
	}

	
	// Load Accumulator Address
	static constexpr Byte INS_LDA_IM  = 0xA9, INS_LDA_ZP  = 0xA5,
	INS_LDA_ZPX = 0xB5;

	void LDASetStatus()
	{
		/*
		 Sets the Zero (Z) flag if the accumulator (A) is zero,
		 and sets the Negative (N) flag based on the most significant bit of A.
		*/
		Z = (A == 0);
		N = (A & 0b10000000) > 0;
	}

	void Execute( u32 Cycles, Mem& memory )
	{
		while (Cycles > 0)
		{
			Byte Ins = FetchByte(Cycles, memory);
			switch (Ins)
			{
			case INS_LDA_IM:
			{
				Byte Value = FetchByte(Cycles, memory);
				A = Value;

				LDASetStatus();
			} break;
			case INS_LDA_ZP:
			{
				// 3 cycles
				Byte ZeroPageAddr = FetchByte(Cycles, memory);
				A = ReadByte(Cycles, ZeroPageAddr, memory);

				LDASetStatus();
			} break;

			default:
			{
				printf("Instruction not handled %d", Ins);
			} break;
			} 
		}
	}

};